import logo from './logo.svg';
import './App.css';
import db from "./firebase"
import { useEffect, useState } from 'react';
import { collection, onSnapshot } from 'firebase/firestore';

export default function App() {

    const [products, setProducts] = useState([])

    useEffect(() => 
        onSnapshot(
            collection(db, "products"),
            (snapshot) => setProducts(snapshot.docs.map(doc => ({id: doc.id, ...doc.data()}) ))
        ), 
        []
    );

    return(
        <div className='main'>
            <button className='big-font-butt' onClick={() => {alert("proverka")}}>Add new</button>
            <ul>
                <li><a href="#">test </a> <Cust color="#00ff00"/> ??</li>
                <li><a href="#">test </a> <Cust color="#ffff00"/> ??</li>
                <li><a href="#">test </a> <Cust color="#ff0000"/> ??</li>
                <li><a href="#">test </a> <Cust color="#0000ff"/> ??</li>
            </ul>

            {
            // <div style={{ 
            //     maxWidth: "1200px",
            //     margin: "0 auto",
            //     display: "grid",
            //     gap: 1,
            //     gridTemplateColumns: "repeat(auto-fit, minmax(300px, 1fr))"
            // }} > 
            }

            <div style={{ 
                width: "100%",
                // maxWidth: "1200px",
                margin: "40px",
                display: "flex",
                flexWrap: "wrap",
            }} >
                { products.map(product => (
                    <ProductCard 
                        key={product.id}
                        name={product.name} 
                        amount={product.amount} 
                        price={product.price}
                    />
                )) }
            </div>
        </div> 
    );
}

function Cust({color}) {
    const style = {
        height: 25,
        width: 25,
        margin: "0px 10px",
        backgroundColor: color,
        borderRadius: "50%",
        display: "inline-block"
    }

    return (
        <span style={style}></span>
    );
}

function ProductCard({name, amount, price}) {
    const cardStyle = {
        // width: "100%",
        
        aspectRatio: "2/1",
        backgroundColor: "white",
        border: "solid 4px darkgrey",
        borderRadius: "16px",
        margin: "32px",
        flex: 1,
        // display: "flex",
        // justifyContent: "center",
        // alignItems: "center",
        padding: "30px",
        
    }
    return (
        <div style={cardStyle}>
            <div>
                <h1>{name}</h1>
                <h3>amount: {amount}</h3>
                <h3>price: {price}</h3>
            </div>
        </div>
    );
}
